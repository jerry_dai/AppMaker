export const state = () => ({
  lang: 'cn',
  error: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.'
})

export const mutations = {
  setLang (state, lang) {
    state.lang = lang
  },
  setError (state, error) {
    state.error = error
  }
}

export const actions = {}

export const getters = {
  lang: state => state.lang
}
